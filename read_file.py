import csv

def read_data():
    rows = []
    csv_file = open('imobiliaria.csv')
    csv_reader = csv.reader(csv_file, delimiter=';')
    for row in csv_reader:
        rows.append(row)
    csv_file.close()
    return rows
    
def filtro(bairro, tipo_filtro):
    infos = read_data()
    colunas = infos[0]
    index = colunas.index(tipo_filtro)
    infos.pop(0)
    if bairro:
        if tipo_filtro == 'valorIptu':
            return [dict(
                bairro=info[14],
                valor=float(info[2]),
                tipo_filtro=float(info[index].replace(',', '.')),
            ) for info in infos if info[14].upper() == bairro.upper()]
        return [dict(
            bairro=info[14],
            valor=float(info[2]),
            tipo_filtro=float(info[index]),
        ) for info in infos if info[14].upper() == bairro.upper()]
    return [dict(
            bairro=info[14],
            valor=float(info[2]),
            tipo_filtro=float(info[index]),
        ) for info in infos]
