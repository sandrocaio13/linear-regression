import tensorflow as tf
import numpy as np

tf_house_size = tf.placeholder(tf.float32, name='house_size')
tf_price = tf.placeholder(tf.float32, name='house_size')

tf_size_factor = tf.Variable(np.random.randn(), name='size_factor')
tf_price_offset = tf.Variable(np.random.randn(), name='price_offset')

tf_price_pred = tf_size_factor * tf_house_size + tf_price_offset
tf_cost = tf.reduce_sum(tf.pow(tf.pow(tf_price_offset - tf_price, 2)) / (2 * num))
