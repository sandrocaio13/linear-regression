from read_file import filtro
from regressao_linear import linear_regression

tipo_filtro = ''
bairro = ''
iniciar = True

print('Regressão Linear')
print('''
1 – Preço x VagasGaragem (Bairro  Alto Barroca)\n
2 - Preço x ÁreaUtil (Bairro  Alto Barroca)\n
3 - Preço x ValorIPTU (Bairro  Alto Barroca)\n
4 - Preço x ÁreaUtil (Todos os Bairros)\n
''')
op = input('Insira a opção: ')

if op == '1':
    tipo_filtro = 'vagaGaragem'
    bairro = 'Alto Barroca'
elif op == '2':
    tipo_filtro = 'areaRealPrivativa'
    bairro = 'Alto Barroca'
elif op == '3':
    tipo_filtro = 'valorIptu'
    bairro = 'Alto Barroca'
elif op == '4':
    tipo_filtro = 'areaRealPrivativa'
else:
    iniciar = False
    print('Por favor, escolha uma opção correta!')

if iniciar:
    data = filtro(bairro, tipo_filtro)
    bairro = bairro or 'Todos os bairros'
    linear_regression(data, tipo_filtro, bairro)
