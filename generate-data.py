import numpy as np
import math
import tensorflow as tf

def normalize(array):
    import pdb; pdb.set_trace()
    teste = (array - array.mean()) / array.std()
    return teste

def normalize_single_value(value, array):
    return (value - array.mean()) / array.std()

def denormalize(value, array):
    return value * array.std() + array.mean()

#gera o tamanho em pés de 160 casas com repetição dos valores aleatórios
import pdb; pdb.set_trace()
num_house = 160
np.random.seed(42)
house_size = np.random.randint(low=95, high=325, size=num_house)

#gerando os preços das casas de acordo com o seu tamanho, com números randomicos
np.random.seed(42)
house_price = house_size * 1000.0 + np.random.randint(low=2000, high=7000, size=num_house)
num_train_samples = math.floor(num_house * 0.7)

#Treinando com 70% dos dados
train_house_size = np.asarray(house_size[:num_train_samples])
train_price = np.asarray(house_price[:num_train_samples])
train_house_size_norm = normalize(train_house_size)
train_price_norm = normalize(train_price)

#Teste com 30% dos dados
test_house_size = np.asarray(house_size[num_train_samples:])
test_house_price = np.asarray(house_price[num_train_samples:]) 

# Modelo para treinamento
tf_house_size = tf.placeholder(tf.float32, name='house_size')
tf_price = tf.placeholder(tf.float32, name='house_size')

tf_size_factor = tf.Variable(np.random.randn(), name='size_factor')
tf_price_offset = tf.Variable(np.random.randn(), name='price_offset')

tf_price_pred = tf_size_factor * tf_house_size + tf_price_offset
tf_cost = tf.reduce_sum(tf.pow(tf_price_pred - tf_price, 2)) / (2 * num_train_samples)

#Machine Learning
learning_rate = 0.1
optimizer = tf.train.GradientDescentOptimizer(learning_rate).minimize(tf_cost)

init = tf.global_variables_initializer()

with tf.Session() as sess:
    sess.run(init)
    num_training_iter = 80
    for iteration in range(num_training_iter):
        for(x, y) in zip(train_house_size_norm, train_house_size_norm):
            print(f'size: {x}, price: {y}')
            sess.run(optimizer, feed_dict={tf_house_size: x, tf_price: y})
    training_cost = sess.run(tf_cost, feed_dict={tf_house_size: train_house_size_norm, tf_price: train_price_norm})
    print ('Trained cost=', training_cost, 'size_factor=', sess.run(tf_size_factor), 'price_offset=', sess.run(tf_price_offset), '\n')

    for (size, price) in zip(test_house_size, test_house_price):
        value = normalize_single_value(size, house_size)
        price_prediction = sess.run(tf_price_pred, feed_dict={tf_house_size:value})
        price_prediction = denormalize(price_prediction, house_price )
        difereca_valor = price_prediction - price
        print(f'''Tamanho da casa: {size}, Preço Original: {price}, Preço sugerido: {price_prediction}, Diferença do valor: {difereca_valor}''')
