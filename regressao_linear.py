import numpy as np
import math
import tensorflow as tf
import locale

def normalize(array):
    return (array - array.mean()) / array.std()

def normalize_single_value(value, array):
    return (value - array.mean()) / array.std()

def denormalize(value, array):
    return value * array.std() + array.mean()

def normalize_tipo_filtro(tipo_filtro):
    if tipo_filtro == 'vagaGaragem':
        return 'Vagas de Garagem'
    if tipo_filtro == 'areaRealPrivativa':
        return 'Área Útil'
    return 'Valor do IPTU'

def normalize_bairro(bairros):
    if bairros:
        return bairros
    return 'Todos os bairros'

#recebe os dados do csv
def linear_regression(data, tipo_filtro, bairros):

    #recebe as informações das casas
    num_house = len(data) 
    house_info = [infos['tipo_filtro'] for infos in data]

    #recebe as informações com os preçoes da casa
    house_price = [infos['valor'] for infos in data]
    num_train_samples = math.floor(num_house * 0.7)

    #Treinando com 70% dos dados
    train_house_info = np.asarray(house_info[:num_train_samples])
    train_price = np.asarray(house_price[:num_train_samples])
    train_house_info_norm = normalize(train_house_info)
    train_price_norm = normalize(train_price)

    #Teste com 30% dos dados
    test_house_info = np.asarray(house_info[num_train_samples:])
    test_house_price = np.asarray(house_price[num_train_samples:]) 

    # Modelo para treinamento, valores que serao usados posteriomente na Machine Learing
    tf_house_info = tf.placeholder(tf.float32, name='house_info')
    tf_price = tf.placeholder(tf.float32, name='house_info')

    # Modelo para treinamento, valores que serao usados posteriomente na Machine Learing
    tf_size_factor = tf.Variable(np.random.randn(), name='size_factor')
    tf_price_offset = tf.Variable(np.random.randn(), name='price_offset')

    #Aqui temos a formula da regressão linear Y = aX + b
    tf_price_pred = tf_size_factor * tf_house_info + tf_price_offset
    #função de perda, usada para otimizar e minimizar o erro do modelo 
    tf_cost = tf.reduce_sum(tf.pow(tf_price_pred - tf_price, 2)) / (2 * num_train_samples)

    #Machine Learning
    #taxa de aprendizado da ML
    learning_rate = 0.1
    #Cada vez que é executado o optimizer, o valor destas variáveis é reprocesso e ajustado, até chegar ao valor mais próximo de ótimo.
    optimizer = tf.train.GradientDescentOptimizer(learning_rate).minimize(tf_cost)

    init = tf.global_variables_initializer()

    with tf.Session() as sess:
        locale.setlocale(locale.LC_ALL, 'pt_BR.UTF-8')
        index = 0
        sess.run(init)
        num_training_iter = 80
        for iteration in range(num_training_iter):
            for(x, y) in zip(train_house_info_norm, train_house_info_norm):
                index += 1
                print(f'index: {index} size: {x}, price: {y}')
                sess.run(optimizer, feed_dict={tf_house_info: x, tf_price: y})
        training_cost = sess.run(tf_cost, feed_dict={tf_house_info: train_house_info_norm, tf_price: train_price_norm})
        print ('Trained cost=', training_cost, 'size_factor=', sess.run(tf_size_factor), 'price_offset=', sess.run(tf_price_offset), '\n')
        
        for (size, price) in zip(test_house_info, test_house_price):
            value = normalize_single_value(size, np.asarray(house_info))
            price_prediction = sess.run(tf_price_pred, feed_dict={tf_house_info:value})
            price_prediction = denormalize(price_prediction, np.asarray(house_price))
            diferenca_valor = price_prediction - price
            price = locale.currency(price)
            price_prediction = locale.currency(price_prediction)
            diferenca_valor = locale.currency(diferenca_valor)
            print(f'''Bairro: {normalize_bairro(bairros)}, {normalize_tipo_filtro(tipo_filtro)}: {size}, Preço Original: {price}, Preço sugerido: {price_prediction}, Diferença do valor: {diferenca_valor}''')
